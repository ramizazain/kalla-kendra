import "./App.css";
import Header from "./components/header/Header";
import MainContainer from "./components/main-container/MainContainer";

function App() {
  return (
    <div className="App">
      <Header />
      <MainContainer />
    </div>
  );
}

export default App;
