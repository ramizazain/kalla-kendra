import React from 'react';
import "./Header.css";

const Header = () => {
  return (
    <div className="App-header">
      <h1>Kalla Kendra </h1>
      <div className="topnav">
        <div className="login-button" role={"button"}>
          Log In
        </div>
        <div className="signup-button" role={"button"}>
          Sign Up
        </div>
      </div>
    </div>
  );
};

export default Header;
