import "./MainContainer.css";
import singer from "../../images/Singer.png";
import speaker from "../../images/speaker.png";
import guitarist from "../../images/guitarist.png";
import photographer from "../../images/photographer.png";
import painter from "../../images/painter.png";
import design1 from "../../images/design1.png";
import design2 from "../../images/design2.png";

const MainContainer = () => {
  return (
    <div className="Main-container">
      <img src={design1} className="designImageLeft" />
      <img src={design2} className="designImageRight" />

      <h1 className="Main-title">Welcome! to Kalla Kendra</h1>
      <div className="buttonStyle">World of Artist</div>
      <div className="images">
        <img src={speaker} className="image" />
        <img src={guitarist} className="image" />
        <img src={painter} className="image" />
        <img src={photographer} className="image" />
        <img src={singer} className="image" />
      </div>
    </div>
  );
};

export default MainContainer;